## CPU 

1. perf + flameGraph

	出现问题， 可能是系统权限限制了， 可以切到 root 尝试。
	
	```
	stackcollapse-perf.pl  --kernel < **.stacks  |  // 如果有perf script
	flamegraph.pl --color=js --hash > flame.svg 
	````
	流程：(use root)
		
		1. perf record -F 频率 -g -P pid -- sleep 时间
		
		2. perf script > **.stacks
		
		3. stackcollapse-perf.pl --kernel < **.stacks| flamegraph.pl --color=js --hash > **.svg
		
		4. python -m http.server

2. --prof + --prof-process/v8-web-ui
	
	v8 内建的性能分析工具
	
	1. node --prof 启动程序
	
	2. node --prof-process --preprocess  生成的 *-v8.log > *.json
	
	3.  v8/tools/profview/index.html   upload 刚才生成的 json 文件。 就可以看到可视化的图表了。
	
	
3. v8-profiler + chromeDevTools/GraphFlame (废弃)

	v8 profiler api
	
	但是 v8-profiler 不支持 node 8.x 版本。
	
