# Fetch API 是基于Promise 设计的. 

###### 语法说明
```
fetch(url, option).then(function(response){
     // handle http response
}).catch(function(error){
     // handle network error
});
```

###### 参数
```
fetch(url, {
	method: 'POST',
	body: 'data',
	headers: {
		'Content-type': 'application/json'
	},
	....
}).then(response=>{
	response.status          // Number 100-599
	response.statusText    // String
	response.headers       // Headers
	response.url              // String
	
	response.text()          // Promise
}).catch(err=>{
	error.message          // String
});
```
- url 定义要获取的资源, 可能是一个 url, 也可能是一个 Request 对象.

- options 一个配置对象.包含所有对请求的设置. 可选参数有:
	1. method: 请求使用的方法. 如 Get/Post.
	2. headers: 请求的头信息. 形式为 Header 对象或者 ByteString
	3. body: 请求的 body 信息. 可能是一个 Blob/BufferSource/FormData/... `需要注意的是, Get 和 Head 方法的请求不能包含 body 信息. `
	4. mode: 请求模式, 如cors/no-cors/same-origin
	5. credentials: 请求的 credentials . 如 omit/same-origin/include
	6. cache : 请求的 cache 模式. 如 default/no-store/reload/no-cache/force-cache/...

- response 一个 Promise. resolve 时, 回传 Response 对象:
	- 属性: 
		1. status(Number) http请求结果参数. 在100-500 之间.
		2. statusText(String) 服务器返回的状态报告.
		3. ok(Boolean) 如果返回200 表示请求成功, 则为 true.
		4. headers(Headers) 返回头部信息. 
		5. url(String): 请求的地址.
		6. responseType 响应的类型
	- 方法: 
		1. text() 以 String 形式生成请求text.
		2. json() 生成 JSON.parse(responseText)的结果
		3. blob() 生成一个 Blob.
		4. arrayBuffer() 生成一个 ArrayBuffer.
		5. formData() 生成格式化的数据. 可以用于其他的请求. 
	- 其他的方法: 
		1. clone() 
		2. Response.error()
		3. Response.redirect()

- response.headers
	- has(name) (Boolean) 判断是否存在该信息头.
	- get(name) (String) 获取信息头的数据.
	- getAll(name) (Array) 获取所有的头部数据
	- append(name, value) 添加 header 的内容.
	- delete(name) 删除 header 的信息
	- forEach(function(value, name){...}, {thisContext}) 循环读取该 header 的信息. 
	
---
## 使用案例
- HTML
```
fetch(url).then(response=>{
	return response.text();
}).then(body=>{
	document.body.innerHTML = body
})
```
- Image
```
let myImage = document.querySelector('img');
fetch('flowers.jpg').then(response=>{
	return response.blob();
}).then(myBlob=>{
	myImage.src=URL.createObjectURL(myBlob);
})
```
- JSON
```
fetch(url).then(response=>{
	return response.json();
}).then(data=>{
	console.log(data);
}).catch(err=>{
	console.log(err);
});
```
- Post 请求
```
fetch(url, {
	method: 'post',
	headers: {
		'Accept': 'application/json',
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		name: 'Hubot',
		login: 'hubot'
	})
});
```



--- 
## Fetch API 提供了一个 fetch 方法. 是W3C 的新的标准. 被定义在了 BOM 的 window 的对象中. 可以用来对远程资源进行访问. 返回一个 Promise 对象.

--- 
## 非常重要的一点 , request和 response 的 body 只能被读取一次. 他们有个属性 BodyUsed 属性. 保证了应用只能消费一次数据.
## 如果要多次读取, 可以使用 clone 方法. 注意, 必须是先 clone 然后在读取.!!
