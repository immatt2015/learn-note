NVM 指定安装源

- NVM_NODEJS_ORG_MIRROR＝镜像地址 nvm install .... 
- 淘宝的镜像源： http://npm.taobao.org/mirrors/node

例子：

> NVM_NODEJS_ORG_MIRROR=http://npm.taobao.org/mirrors/node nvm install v6.5.0

-

> https://npm.taobao.org/mirrors