### rabbitmq ttl 

rabbitmq 可以针对 msg 和 queue 设置超时时间. 通过 queue arguments 或者 polices(推荐) 实现. 超时可以针对一条消息/一堆消息

ttl 设置也可以通过 operator polices 强制设置.

但是有个问题, polices 是用过配置文件进行设置的, 在集群里面不是太合适.

服务器可以确保, dead message 不会被 basic.deliver 投送给消费者/被获取到. 服务器会在过期之后很短时间内移除消息.

ttl 参数或者策略值需要为正数(单位为毫秒). 以为这消息会在 queue 里存在一段时间,或者消息被发送给消费者了. 类型为 amqp 0-9-1 类型.

###### 具体方法:
1. 使用 polices 设置超时, 可以通过 rabbitmqctl
2. 声明队列的时候, 使用 x-arguments 设置 ttl.


channel 实际上与 queue/exchange 都是不直接绑定的. 使用的时候, 直接传入对应的值.

#### 延迟队列, [参考](https://segmentfault.com/a/1190000013311069)
需要两个条件: 

1. rabbitmq_delayed_message_exchange 插件, [地址](https://github.com/rabbitmq/rabbitmq-delayed-message-exchange)  [启用插件](https://www.rabbitmq.com/plugins.html)

	```
rabbitmq-plugins list // 已经安装了 rabbitmq_delayed_message_exchange 插件
rabbitmq-plugins enable rabbitmq_delayed_message_exchange
rabbitmq-plugins list
	```
2. 设置 ttl 


具体代码:
```
import amqplib from 'amqplib';
const queueName = 'test-queue';
const exclusive = false;
const durable = false;
const autoDelete = false;

const levels = ['info', 'error', 'test'];
async function publisher() {
    const exchangeName = 'test-exchange';
    const conn = await amqplib.connect('amqp://localhost:5672');

    const ch = await conn.createChannel();
    const e = await ch.assertExchange(
        exchangeName,
        'x-delayed-message', // exchange 类型, 需要配合 arguments.x-delayed-type=direct
        {
            arguments: {
                'x-delayed-type': "direct"
            },
            durable,
            autoDelete,
        });

    const qs = await Promise.all(levels.map(async l => {
        const q = await ch.assertQueue(
            `${l}-queue`,
            {
                exclusive,
                durable,
                autoDelete
            });
            console.log('l', l)
        await ch.bindQueue(q.queue, e.exchange, l);
        return q.queue;
    }));
console.log(qs);
    for (const qq of levels) {
        await ch.publish(
            e.exchange, // exchange
            qq,         // routekey
            Buffer.from(qq + ' >>  hello world'), // content
            {
                headers: { "x-delay": 4000 }  // options
            });
    }


    // let [e, q] = await Promise.all([
    //     ch.assertExchange(
    //         exchangeName,   // exchange 名字
    //         'x-delayed-message', // exchange 类型, 需要配合 arguments.x-delayed-type=direct
    //         {
    //             arguments: {
    //                 'x-delayed-type': "direct"
    //             },
    //             durable,
    //             autoDelete,
    //         }),
    //     ch.assertQueue(
    //         queueName,
    //         {
    //             exclusive,
    //             durable,
    //             autoDelete
    //         }),
    // ]);
    // await ch.bindQueue(q.queue, e.exchange, 'error');

    // await ch.publish(
    //     e.exchange,
    //     'info',
    //     Buffer.from(`helll world! router: 'error'  ${new Date()}`),
    //     { headers: { "x-delay": 5000 } },
    // );
    // await new Promise(r => setTimeout(r, 1000));
    // console.log('==== 应该过期了');
    console.log('==== 消息发送完毕');
}


// 消费者
async function consumer() {
    const conn = await amqplib.connect('amqp://localhost:5672');
    conn.on('error', console.error);

    const ch = await conn.createChannel();

    for (const l of levels) {
        console.log('l => ', l)
        const q1 = await ch.assertQueue(`${l}-queue`, { exclusive, durable, autoDelete });

        ch.consume(
            q1.queue,
            c => c && console.error(c.content.toString(), new Date()),
            {
                noAck: true
            });
    }
}

void async function () {
    await consumer();
    console.log('==== 监听 ok');
    await publisher();
    await   消息撤回
}().catch(console.error);
```



安装 erlang  https://hostpresto.com/community/tutorials/how-to-install-erlang-on-ubuntu-16-04/

安装 rabbitmq, 
```
cd /tmp/
wget https://packages.erlang-solutions.com/erlang-solutions_1.0_all.deb
sudo dpkg -i erlang-solutions_1.0_all.deb
sudo apt-get update -y
sudo apt-get install erlang -y
```
安装完毕, 使用 systemctl 管理


添加远程用户
```
rabbitmqctl add_user test test
rabbitmqctl set_user_tags test administrator
rabbitmqctl set_permissions -p / test ".*" ".*" ".*"
```


## 多租户访问
rabbitmq 的多租户是通过 vhost 实现的。每个 vhost 相当于单个独立的`rabbitmq`实例。
```
rabbitmqctl add_vhost  vhost-name
rabbitmqctl list_vhost 
rabbitmqctl set_permissions -p  vhost-name user-name  ".*" ".*" ".*"
```
默认新创建的vhost 用户是没法访问的。 通过授权就可以访问。

连接的时候只需要在连接字符串的后面加上一个路径 `/vhost-name` 就可以访问