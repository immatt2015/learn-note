## 关闭 thp 内存大页

- 使用systemctl 关闭
    ```
    vi /etc/systemd/system/disable-thp.service
    
    [Unit]
    Description=Disable Transparent Huge Pages (THP)
    
    [Service]
    Type=simple
    ExecStart=/bin/sh -c "echo 'never' > /sys/kernel/mm/transparent_hugepage/enabled && echo 'never' > /sys/kernel/mm/transparent_hugepage/defrag"
    
    [Install]
    WantedBy=multi-user.target
    
    ```
    
    ```
    sudo systemctl daemon-reload
    sudo systemctl start disable-thp
    sudo systemctl enable disable-thp
    ```

-  使用 tuned 和 ktune



## 打开文件句柄最大数量
- systemctl
    ```
        LimitNOFILE=65536
    ```
    ```
    sudo systemctl daemon-reload
    sudo systemctl restart redis.service
    ```
    检查修改结果
    ```
    cat /run/redis/redis-server.pid
    cat /proc/PID/limits
    ```
- init.d
    ```
    ulimit -S -n 102400
    ```




## systemctl限制资源
```
[Service]
# Other directives omitted
# (file size)
LimitFSIZE=infinity
# (cpu time)
LimitCPU=infinity
# (virtual memory size)
LimitAS=infinity
# (locked-in-memory size)
LimitMEMLOCK=infinity
# (open files)
LimitNOFILE=64000
# (processes/threads)
LimitNPROC=64000

```