### hostnamectl命令用于查看当前主机的信息。


##### 显示当前主机的信息
> hostnamectl

##### 设置主机名。
> sudo hostnamectl set-hostname rhel7

### localectl
> localectl命令用于查看本地化设置。


##### 查看本地化设置
> localectl

##### 设置本地化参数。
> sudo localectl set-locale LANG=en_GB.utf8

> sudo localectl set-keymap en_GB