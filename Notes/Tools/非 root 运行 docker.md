## 非 root 运行 docker.

一般, docker 需要通过 unix-socket 通信, 所以需要 sudo 权限, 

但是服务器上使用 root 权限运行是个问题, 需要使用非 root 角色运行. 只要把相应的用户加到对用的组就好.

> usermod -aG docker user

