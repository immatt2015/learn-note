## nginx 多域名
### nginx 仅域名访问
```
server {
	listen 80;
	server_name _ default_server;
	return 500;
}

server {
	listen 80;
	server_name xxx;
	*****
}


```

当所有server的规则都不匹配时，nginx会采用第一条server配置，所以一般第一条server会使用阻止页面。

## nginx 开启鉴权

```
server {
    listen  80;
    server_name xxxx;
    
    location / {
    
          auth_basic "auth";
          auth_basic_user_file conf.d/htpasswd;
          autoindex on;
          
          // proxy_pass http:///127.0.0.1;
          // or  other behavior
          
    }
}
```

密码使用 htpasswd -nb username userpass 产生

## nginx websocket转发
websocket 转发方式与 http 差别不大, 只是需要设置特定的头部
```
proxy_set_header X-Real-IP $remote_addr;
proxy_set_header Host $host;
proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

proxy_http_version 1.1;
proxy_set_header Upgrade $http_upgrade;
proxy_set_header Connection "upgrade";
```




### nginx 缓存

还没找到如何根据不同路径甚至不同过期时间
```
proxy_cache_path /var/cache/nginx levels=1:2 keys_zone=imgcache:100m inactive=2h max_size=1g;
map $uri $use_cache {
   default 0;
   ~^/index  1;
   ~^/test 5;
}
server {
    listen       3013;
    server_name  localhost;
    
    location / {
        set $cache_pos off;

        if ( $use_cache != 0) {
         set $cache_pos imgcache;
        }

        proxy_pass http://$ip:8990;

        proxy_cache_methods GET POST;
        proxy_cache $cache_pos;
        proxy_cache_min_uses 1;
        proxy_cache_lock on;
        proxy_cache_key   $uri;
        # proxy_cache_valid 200 302 $use_cache;
        proxy_cache_valid 200 302 1m;
        proxy_cache_lock_timeout 5s;

        proxy_cache_background_update on;
        proxy_cache_use_stale error timeout updating http_500 http_502 http_503 http_504;

        proxy_ignore_headers Set-Cookie Cache-Control;
        proxy_hide_header Cache-Control;
        proxy_hide_header Set-Cookie;
        
    }
}
```