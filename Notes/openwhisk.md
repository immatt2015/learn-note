## 搭建

```
git clone https://github.com/apache/incubator-openwhisk.git

export OPENWHISK_TMP_DIR=/data/openwhisk

 ./gradlew distDocker
 
 
```

如果遇到docker `RUN apt update` 的错误,先核对服务器时间, 也可能是服务器商家屏蔽了:
```
systemctl  status ntpd
systemctl  stop ntpd
sudo date -s "$(wget -qSO- --max-redirect=0 google.com 2>&1 | grep Date: | cut -d' ' -f5-8)Z"
systemctl  start ntpd
```

