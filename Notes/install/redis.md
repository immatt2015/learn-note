### 旧版本

```
yum install epel-release 
yum upate 
yum install redis

systemctl status redis
```

### 新版本

```
wget http://download.redis.io/releases/redis-${version}.tar.gz

tar zvxf redis-${version}.tar.gz
yum install gcc
cd redis-${version} && make && make install
sh ./util/install_server.sh

## 配置 service
## 配置 redis.conf
```
