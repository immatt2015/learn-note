## mongodb 

https://docs.mongodb.com/manual/tutorial/install-mongodb-on-red-hat/

写入 repo

`yum install mongo-org`

#### 优化点:
```
 echo never > /sys/kernel/mm/transparent_hugepage/defrag
 echo never > /sys/kernel/mm/transparent_hugepage/enabled
```

如果要持久化, 需要 在 `/etc/rc.local` 指定

注意修改 /etc/local/rc.local 的权限, /etc/rc.local 里面有提示

-------

集群:

1. 准备机器

2. 启动机器

3. 连接 mongo 配置副本集, 
```
replaction:
	oplogSizeMB: 5120
	replSetName: xxxx
	keyFile: XXXX/keyFile
```

4. 主节点创建用户权限

5. 创建 副本集认证 key 
`
openssl rand -base64 90 -out dir  mongodb/keyfile/keyfile
`
6. copy key (600 权限)

7. 关闭每个节点

8. 开启 auth

9. 重启 mongo

www.imooc.com/article/43509


http://www.runoob.com/mongodb/mongodb-replication.html


操作步骤:

1. 安装 , 开放端口, ip replSetName

2. 登录其中一台, 配置 `rs.initialize({_id: replSetName, members: [{_id: 0, host: 'ip:port'}, ...]})`

3. rs.status 

4. 设置 username pwd 

5. 设置 security.keyFile (mode: 0600), authorization: enable

6. 重启

