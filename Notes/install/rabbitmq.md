### Rabbitmq 

```
## rpm 安装

wget https://dl.bintray.com/rabbitmq/all/rabbitmq-server/3.7.7/rabbitmq-server-3.7.7-1.el6.noarch.rpm

rpm -ihv rabbitmq.rpm

https://github.com/rabbitmq/erlang-rpm
## 编辑文件
# In /etc/yum.repos.d/rabbitmq-erlang.repo
[rabbitmq-erlang]
name=rabbitmq-erlang
baseurl=https://dl.bintray.com/rabbitmq/rpm/erlang/21/el/7
gpgcheck=1
gpgkey=https://dl.bintray.com/rabbitmq/Keys/rabbitmq-release-signing-key.asc
repo_gpgcheck=0
enabled=1


# 源自 https://github.com/rabbitmq/erlang-rpm

```
安装 erlang

`yum install rabbitmq-erlang`
 
如果上面的走不通, 直接下载 rpm 进行安装  from github

如果之前安装了erlang， 可能需要彻底删除以前的安装

可能过程中也要安装 socat

