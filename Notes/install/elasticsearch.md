### elastcisearch 

`wget rpm package `

`rpm -ihv .rpm`

### java 

jdk1.8-openjdk

`yum install java`


### 集群设置

1. 安装 es
2. 修改配置: 

```
# vi /etc/elasticsearch/elasticsearch.yml

# 统一的集群名
cluster.name: syncwt-es
# 当前节点名
node.name: syncwt-es-node-1
# 对外暴露端口使外网访问
network.host: 0.0.0.0
# 对外暴露端口
http.port: 9200
# ...还有很多可以设置，这些是基础的。具体看上面的配置参数说明
discovery.zen.ping.unicast.hosts: [] # 可用的节点

cluster.master: true
cluster.data: true
```

3. 查询集群状态 curl xxxx:9200/_cluster/stats?pretty

