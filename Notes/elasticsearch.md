## 返回字段

- stored_fields 从 mapping 中进行查询. 一般不推荐
- source: []  返回特定的字段, 也可以包含一个 include: [], exclude: []

'


# Filebeat 
filebeat 收集日志的时候, 是根据文件的 inode 这个标识进行文件区分的, (这个标识不会因为文件移动/重命名进行改变); 只要设置合理的 log rotate 大小, 日志收集间隔, 这样就不会丢数据.

日志收集间隔时间内产生的日志大小 < 单个日志文件大小 * 日志文件数量(备份数量 + 1)

关闭 harvester 意味着关闭 filehandler。
如果在harvester关闭后，文件又更新了，它会在 scan_frequency 指定的时间间隔后再次被采集。
当然，如果 在harvester 关闭期间，文件被移动或删除，Filebeat将不能再次采集该文件，数据会丢失。

https://www.jianshu.com/p/b7245ce58c6a

句柄关闭时间, 还要跟实际符合, 这样才不会造成资源的浪费


## 绑定局域网网卡
`
network.host: 192.168.50.23

transport.host: localhost
`