### use diy ssh-key

1. 生成一个新的密钥, 并添加公钥到 git**.
2. 本地编辑 ~/.ssh/config 文件, 添加

```
Host git**.com
        HostName git**.com
        IdentityFile 密钥位置
```

(仅限 *nix 系统)

3. `ssh -T git@git**.com`, 如果出现 welcome, *** 就说明可用了