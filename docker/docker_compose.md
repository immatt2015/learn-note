## docker

关机重启后， 如果出现无法启动，检查 `sysetmctl cat docker` 如果服务被 masked， 可以使用 `sysetmctl unmask docker`，启动docker即可。

docker top === docker-top


## docker-compose 安装

##### 设置环境变量 ALL_POROXY=sock5h:xxxx 使用代理，安装docker-compose

简单的方式 apt-get install docker-compose

## docker-compose 使用

- build
- kill
- logs
- port
- run
- scale

## docker-compose  yml 模版

- image
- build: 指定 dockerfile 路径
- command 覆盖启动后默认执行的命令
- links 链接其他容器服务，格式 service:alias， 通过 hosts
- external_links 链接到docker-compose 外部的容器， 并非compose管理的容器
- ports 暴露的端口信息 格式 host:container 
- expose 暴露端口， 仅针对服务之间
- volumes 卷挂载路径设置。 格式 host:container :ro (read only)
- volumes_from 从其他服务挂载
- environment 设置环境变量，可以用数组或字典的方式
- env_file 从文件中获取环境变量
- extends 基于已有服务进行扩展，
  ```
  web:
    extends:
        file: xxxx.yml
        service: xxxxService
  ```
- dns 
- cap_add/cap_drop 添加/放弃容器的linux能力
- privileged: true
- mem_limit: byte
- cpu_shares
- restart
- entrypoint
- tty
- stdin_open
  

## tip

- docker-compose run  没有no-create 这个选项， 所以运行时， 加 --rm 
- 